﻿using System;
using System.Threading.Tasks;

namespace ChenzaoMel.Interface.Util {
    public interface ILogger {

        Task ErrorAsync(Exception ex, string callerName="", int lineNumber=0);
        Task WarnAsync(Exception ex, string callerName="", int lineNumber=0);
        Task DebugAsync(Exception ex, string callerName="", int lineNumber=0);
        Task InfoAsync(Exception ext, string callerName="", int lineNumber=0);
        Task ErrorAsync(string content, string callerName="", int lineNumber=0);
        Task WarnAsync(string content, string callerName="", int lineNumber=0);
        Task DebugAsync(string content, string callerName="", int lineNumber=0);
        Task InfoAsync(string content, string callerName="", int lineNumber=0);
        void Error(Exception ex, string callerName="", int lineNumber=0);
        void Warn(Exception ex, string callerName="", int lineNumber=0);
        void Debug(Exception ex, string callerName="", int lineNumber=0);
        void Info(Exception ext, string callerName="", int lineNumber=0);
        void Error(string content, string callerName="", int lineNumber=0);
        void Warn(string content, string callerName="", int lineNumber=0);
        void Debug(string content, string callerName="", int lineNumber=0);
        void Info(string content, string callerName="", int lineNumber=0);
    }
}
