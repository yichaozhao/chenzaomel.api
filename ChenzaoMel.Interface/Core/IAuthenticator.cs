﻿
using ChenzaoMel.Model.Core;
using ChenzaoMel.Model.DataTransfer;

namespace ChenzaoMel.Interface.Core {
    public interface IAuthenticator
    {
         string GenerateSignature(AuthRequest request);
    }
}
