﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Xml;
using ChenzaoMel.Api.ApiUtil;
using ChenzaoMel.Api.Attribute;
using ChenzaoMel.BusinessLogic;
using ChenzaoMel.BusinessLogic.Core;
using ChenzaoMel.BusinessLogic.Util;
using ChenzaoMel.DataAccess;
using ChenzaoMel.DataAccess.Facade;
using ChenzaoMel.Interface.Core;
using ChenzaoMel.Interface.Util;
using ChenzaoMel.Model.Core;
using ChenzaoMel.Model.Core.Exception;
using ChenzaoMel.Model.DataTransfer;
using Swashbuckle.Swagger.Annotations;
using Configuration = System.Configuration.Configuration;
namespace ChenzaoMel.Api.Controllers {

    /*
    11/07/2016 v1.0.0   EZ  Create the project and create wechat controller
    12/07/2016 v1.0.1   EZ  Add daily goal test generate feature
    13/07/2016 v1.0.2   EZ  Support traditional Chinese for command line
    16/07/2016 v1.0.3   EZ  Add new endpoint to retrieve objective details
    29/08/2016 v1.0.4   EZ  Put a locker to the processing method
    02/09/2016 v1.0.5   EZ  Optimize the AddOrUpdateDailyObjective method to avoid cocurrency issue
    04/09/2016 v1.0.6   EZ  Remove the locker

         */

    public class WechatController : AbsApiController {
        private const string DATE_PATTERN = "dd/MM/yyyy";
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK)]
        [Route("~/wechat/test", Name = "WechatPostTest")]
        public HttpResponseMessage WechatPostTest() {
            var model = new WeChatTextMsg() {
                FromUserName = "testuser",
                MsgType = MsgType.Text,
                Content = "test",
                //                CreateTime = WechatHttpUtil.GetCreateTime(),
                ToUserName = "testuser"
            };

            return WechatHttpUtil.GetWechatXmlResponseMessage(model.GenerateXml());
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK)]
        [Route("~/wechat", Name = "WechatPost")]
        public async Task<HttpResponseMessage> WechatPost() {

            //        public async Task<HttpResponseMessage> WechatPost() {
            var logger = Logger.ObtainServerLogger(GetIpAddress());
            var requestContent = Request.Content.ReadAsStreamAsync().Result;
            try {
                //从正文参数中加载微信的请求参数
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(requestContent);

                MsgType msgType;
                Enum.TryParse(xmlDoc.SelectSingleNode("xml/MsgType").InnerText, true, out msgType);
                WeChatTextMsg incomeMsg = new WeChatTextMsg() {
                    FromUserName = xmlDoc.SelectSingleNode("xml/ToUserName").InnerText,
                    Content = xmlDoc.SelectSingleNode("xml/Content").InnerText,
                    CreateTime = long.Parse(xmlDoc.SelectSingleNode("xml/CreateTime").InnerText),
                    ToUserName = xmlDoc.SelectSingleNode("xml/FromUserName").InnerText,
                    MsgType = msgType
                };
                var fromUser = incomeMsg.ToUserName;

                //29/08/2016 v1.0.4   EZ  Put a locker to the processing method
                try {
                    if (LockerHelper.CheckProcess(fromUser)) {
                        await logger.ErrorAsync($"Skip request for user {fromUser}.");
                        return await WechatHttpUtil.GetWechatXmlResponseMessageAsync(new WeChatTextMsg() {
                            FromUserName = incomeMsg.FromUserName,
                            MsgType = MsgType.Text,
                            Content = "正在处理指令，等稍后。",
                            CreateTime = WechatHttpUtil.GetCreateTime(),
                            ToUserName = incomeMsg.ToUserName
                        }.GenerateXml());
                    }
                    await logger.InfoAsync($"Processing request for user {fromUser}.");

                    //                    logger.Info("Checking the post processing status.");
                    //                    var cacheMsg = (IDictionary<string, WeChatTextMsg>)MemoryCacher.GetValue("locker");
                    //                    if (cacheMsg == null)
                    //                    {
                    //                        // Not cache yet
                    //                        logger.Info("The first request.");
                    //                        cacheMsg = new Dictionary<string, WeChatTextMsg>(); // Get the cached message for user
                    //                        cacheMsg.Add(fromUser, incomeMsg);
                    //                    } 


                    //                    MemoryCacher.Add("locker", retMsg, DateTimeOffset.UtcNow.AddHours(1));  // Save to cache

                    // Put the locker
                    //29/08/2016 v1.0.4   EZ  Put a locker to the processing method
                    LockerHelper.PutLocker(fromUser);
                    WeChatTextMsg retMsg = await new WechatMessageService(logger).ProcessText(incomeMsg);
                    //29/08/2016 v1.0.4   EZ  Put a locker to the processing method
                    LockerHelper.RemoveLocker(fromUser);
                    // Remove the locker

                    //                    cacheMsg.Remove(fromUser);  // Remove the locker cache
                    //                    MemoryCacher.Add("locker", retMsg, DateTimeOffset.UtcNow.AddHours(1));  // Save to cache
                    return await WechatHttpUtil.GetWechatXmlResponseMessageAsync(retMsg.GenerateXml());
                } catch (ChenzaoException exMsg) {
                    var model = new WeChatTextMsg() {
                        FromUserName = incomeMsg.FromUserName,
                        MsgType = MsgType.Text,
                        Content = exMsg.Message,
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        ToUserName = incomeMsg.ToUserName
                    };
                    LockerHelper.RemoveLocker(incomeMsg.ToUserName);
                    return await WechatHttpUtil.GetWechatXmlResponseMessageAsync(model.GenerateXml());
                } catch (Exception ex) {
                    var model = new WeChatTextMsg() {
                        FromUserName = incomeMsg.FromUserName,
                        MsgType = MsgType.Text,
                        Content = ex.ToString(),
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        ToUserName = incomeMsg.ToUserName
                    };

                    return await WechatHttpUtil.GetWechatXmlResponseMessageAsync(model.GenerateXml());
                }
            } catch (Exception ex) {
                logger.Error(ex);

            }
            return await Task.Factory.StartNew(() =>
                                             new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent("success", Encoding.UTF8) }
                                         );
        }

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK)]
        [Route("~/wechat/objectives", Name = "WechatObjectives")]
        public async Task<ICollection<ObjectiveResponse>> GetObjectives(string dateFrom, string dateTo) {
            string ipAddress = GetIpAddress();
            Logger logger = Logger.ObtainServerLogger(ipAddress);
            try {
                DateTime from = DateTime.ParseExact(dateFrom, DATE_PATTERN, CultureInfo.InvariantCulture);
                DateTime to = DateTime.ParseExact(dateTo, DATE_PATTERN, CultureInfo.InvariantCulture);
                using (var db = new ChenzaoContext()) {
                    return await new UserObjectiveService(db).ReadObjectiveRange(from, to);
                }
            } catch (Exception ex) {
                await logger.ErrorAsync(ex);
                return new List<ObjectiveResponse>();
            }

        }

        [WXAuthorize]
        [SwaggerResponse(HttpStatusCode.OK)]
        [HttpGet]
        [Route("~/wechat", Name = "WeChatAuth")]
        public async Task<HttpResponseMessage> WeChatAuth() {

            ILogger logger = Logger.ObtainServerLogger(GetIpAddress());
            logger.Info("Wechat Get");
            try {
                var requestQueryPairs = Request.GetQueryNameValuePairs().ToDictionary(k => k.Key, v => v.Value);
                return WechatHttpUtil.GetWechatAuthResponseMessage(requestQueryPairs["echostr"]);
            } catch (Exception ex) {
                logger.Error(ex);
                return await WechatHttpUtil.GetWechatAuthResponseMessageAsync(ex.Message);
            }
        }
    }
}