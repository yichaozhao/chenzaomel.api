﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using ChenzaoMel.BusinessLogic.Core;
using ChenzaoMel.BusinessLogic.Util;
using ChenzaoMel.Interface.Core;
using ChenzaoMel.Model.Core;
using ChenzaoMel.Model.DataTransfer;

namespace ChenzaoMel.Api.Attribute {
    public class WXAuthorizeAttribute : AuthorizeAttribute
    {

        private string _token = ConfigurationManager.AppSettings["Token"];

        protected override bool IsAuthorized(HttpActionContext actionContext) {
                Logger.ObtainServerLogger("").Info($"IsAuthorized: {actionContext.Request.Method.Method}");
            var requestQueryPairs = actionContext.Request.GetQueryNameValuePairs().ToDictionary(k => k.Key, v => v.Value);
            if (requestQueryPairs.Count == 0
                || !requestQueryPairs.ContainsKey("timestamp")
                || !requestQueryPairs.ContainsKey("signature")
                || !requestQueryPairs.ContainsKey("nonce")) {
                return false;
            }


            AuthRequest request = new AuthRequest() { SaltedString = requestQueryPairs["nonce"], Timestamp = requestQueryPairs["timestamp"], Token = _token };
            IAuthenticator authenticator = new WechatAuthenticator();
            string generatedSig = authenticator.GenerateSignature(request);
//            logger.Info(
//                        $"singature: {signature}, dateTime = {timestamp}, nonce = {nonce}, echostr = {echostr}, generatedSignature = {generatedSig}");
            return string.Equals(requestQueryPairs["signature"].ToLower(), generatedSig.ToLower());
        }

        /// <summary>
        /// 处理未授权请求
        /// </summary>
        /// <param name="actionContext">上下文</param>
        protected sealed override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
                Logger.ObtainServerLogger("").Info($"HandleUnauthorizedRequest: {actionContext.Request.Method.Method}");
            actionContext.Response = actionContext.Request.CreateResponse(
                HttpStatusCode.Unauthorized, new { status = "sign_error" });
        }
    }
}