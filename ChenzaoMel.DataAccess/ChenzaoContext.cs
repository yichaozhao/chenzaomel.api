using ChenzaoMel.Model.Base;
using ChenzaoMel.Model.Database;

namespace ChenzaoMel.DataAccess {
    using Model.Core;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ChenzaoContext : DbContext {
        // Your context has been configured to use a 'ChenzaoContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ChenzaoMel.DataAccess.ChenzaoContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ChenzaoContext' 
        // connection string in the application configuration file.

        public ChenzaoContext()
            : base("name=ChenzaoContext") {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Period> Periods { get; set; }
        //        public virtual DbSet<PeriodicalObjective> PeriodicalObjectives { get; set; }
        public virtual DbSet<Objective> Objectives { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PeriodicalObjective>().HasRequired(s => s.Period).WithRequiredPrincipal(
                                                                                                        s =>
                                                                                                        s
                                                                                                            .PeriodicalObjective);
            modelBuilder.Entity<DailyObjectiveDetail>().ToTable("DailyObjectiveDatail");
            modelBuilder.Entity<PeriodicalObjective>().ToTable("PeriodicalObjective");
            modelBuilder.Entity<DailyObjective>().ToTable("DailyObjective");
            modelBuilder.Entity<DailyObjective>().HasMany(s => s.DailyObjectiveDetails).WithRequired(
                                                                                                     s =>
                                                                                                     s.DailyObjective);
        }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}