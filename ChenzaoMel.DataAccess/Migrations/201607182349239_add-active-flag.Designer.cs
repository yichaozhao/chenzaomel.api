// <auto-generated />
namespace ChenzaoMel.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addactiveflag : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addactiveflag));
        
        string IMigrationMetadata.Id
        {
            get { return "201607182349239_add-active-flag"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
