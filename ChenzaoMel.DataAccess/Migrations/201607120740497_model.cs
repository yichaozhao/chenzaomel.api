namespace ChenzaoMel.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "IpAddress");
            DropColumn("dbo.Users", "TimeStamp");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "TimeStamp", c => c.DateTime(nullable: false));
            AddColumn("dbo.Users", "IpAddress", c => c.String());
        }
    }
}
