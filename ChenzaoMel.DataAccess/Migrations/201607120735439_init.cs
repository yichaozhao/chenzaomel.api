namespace ChenzaoMel.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Content = c.String(),
                        ErrorLevel = c.Int(nullable: false),
                        IpAddress = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Objectives",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Content = c.String(),
                        Complete = c.Boolean(nullable: false),
                        IpAddress = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        IpAddress = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Periods",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        IpAddress = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PeriodicalObjective", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.PeriodicalObjective",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Objectives", t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.Id)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DailyObjectiveDatail",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        DailyObjectiveId = c.Long(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Objectives", t => t.Id)
                .ForeignKey("dbo.DailyObjective", t => t.DailyObjectiveId)
                .Index(t => t.Id)
                .Index(t => t.DailyObjectiveId);
            
            CreateTable(
                "dbo.DailyObjective",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Date = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                        AchievementLevel = c.Int(),
                        HappynessLevel = c.Int(),
                        PersonalFeeling = c.String(),
                        Luckiness = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Objectives", t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.Id)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DailyObjective", "UserId", "dbo.Users");
            DropForeignKey("dbo.DailyObjective", "Id", "dbo.Objectives");
            DropForeignKey("dbo.DailyObjectiveDatail", "DailyObjectiveId", "dbo.DailyObjective");
            DropForeignKey("dbo.DailyObjectiveDatail", "Id", "dbo.Objectives");
            DropForeignKey("dbo.PeriodicalObjective", "UserId", "dbo.Users");
            DropForeignKey("dbo.PeriodicalObjective", "Id", "dbo.Objectives");
            DropForeignKey("dbo.Periods", "Id", "dbo.PeriodicalObjective");
            DropIndex("dbo.DailyObjective", new[] { "UserId" });
            DropIndex("dbo.DailyObjective", new[] { "Id" });
            DropIndex("dbo.DailyObjectiveDatail", new[] { "DailyObjectiveId" });
            DropIndex("dbo.DailyObjectiveDatail", new[] { "Id" });
            DropIndex("dbo.PeriodicalObjective", new[] { "UserId" });
            DropIndex("dbo.PeriodicalObjective", new[] { "Id" });
            DropIndex("dbo.Periods", new[] { "Id" });
            DropTable("dbo.DailyObjective");
            DropTable("dbo.DailyObjectiveDatail");
            DropTable("dbo.PeriodicalObjective");
            DropTable("dbo.Periods");
            DropTable("dbo.Users");
            DropTable("dbo.Objectives");
            DropTable("dbo.Logs");
        }
    }
}
