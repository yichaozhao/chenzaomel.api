namespace ChenzaoMel.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addactiveflag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Objectives", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Periods", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Periods", "IsActive");
            DropColumn("dbo.Objectives", "IsActive");
            DropColumn("dbo.Logs", "IsActive");
        }
    }
}
