﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.DataAccess.Core;
using ChenzaoMel.DataAccess.Entity;
using ChenzaoMel.Model.Core.Exception;
using ChenzaoMel.Model.Database;
using ChenzaoMel.Model.DataTransfer;

namespace ChenzaoMel.DataAccess.Facade {
    public class UserObjectiveService : DalService {
        public async Task<User> ReadUserAllObjectives(string id, DateTime dateTime) {
            var user = await new UserService(_context).ReadUser(id);
            if (user == null) {
                throw new ChenzaoException("亲，你还木有设定目标哦。");
            }
            user.CurrentDailyGoals = await new DailyObjectiveService(_context).ReadCurrentDailyGoals(user, dateTime);
            user.CurrentPeriodicalGoal = await new PeriodicObjectiveService(_context).ReadPeriodicalObjective(user, dateTime);
            return user;
        }
        public async Task<User> ReadUserDailyObjectives(string id, DateTime dateTime) {
            var user = await new UserService(_context).ReadUser(id);
            if (user == null) {
                // Create new user
                user = new User() { Id = id };
                _context.Users.Add(user);
                await _context.SaveChangesAsync();

            }
            user.CurrentDailyGoals = await new DailyObjectiveService(_context).ReadCurrentDailyGoals(user, dateTime);
            //           user.CurrentPeriodicalGoal = await new PeriodicObjectiveService().ReadPeriodicalObjective(user);
            return user;
        }
        public async Task<User> ReadUserPerioidicObjectives(string id, DateTime dateTime) {
            var user = await new UserService(_context).ReadUser(id);
            //           user.CurrentDailyGoals = await new DailyObjectiveService().ReadCurrentDailyGoals(user);
            user.CurrentPeriodicalGoal = await new PeriodicObjectiveService(_context).ReadPeriodicalObjective(user, dateTime);
            return user;
        }

        public UserObjectiveService(ChenzaoContext context) : base(context) {
        }

        public async Task<ICollection<ObjectiveResponse>> ReadObjectiveRange(DateTime dateFrom, DateTime dateTo) {
            DailyObjectiveService service = new DailyObjectiveService(_context);
            IList<ObjectiveResponse> ret = new List<ObjectiveResponse>();
            foreach (var user in _context.Users.ToList()) {
            DateTime tempDate = dateFrom;
                while (tempDate <= dateTo) {
                    var response = new ObjectiveResponse {
                        Name = user.Name,
                        UserId = user.Id,
                        Date = tempDate.ToString("dd/MM")
                    };
                    var dailyObjectives = await service.ReadCurrentDailyGoals(user, tempDate);
                    if (dailyObjectives == null) {
                        tempDate = tempDate.AddDays(1);
                    } else {
                        response.NoOfObjectives = dailyObjectives.DailyObjectiveDetails.Count;
                        response.NoOfCompletes = dailyObjectives.DailyObjectiveDetails.Count(s => s.Complete);
                        tempDate = tempDate.AddDays(1);
                        ret.Add(response);
                    }

                }
            }
            return ret;

        }
    }
}
