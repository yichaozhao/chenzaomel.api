﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.Model.Database;
using ChenzaoMel.Model.Util;

namespace ChenzaoMel.DataAccess.Core {
   public class DailyObjectiveService :DalService{
       public async Task<DailyObjective> ReadCurrentDailyGoals(User user, DateTime dateTime)
       {
               var time = dateTime.Date.ToMelDateTime();
               return await 
                   _context.Objectives.OfType<DailyObjective>().Include("DailyObjectiveDetails").Where(
                                                                s =>
                                                                s.UserId == user.Id &&
                                                                 time == s.Date).FirstOrDefaultAsync();
       }

       public DailyObjectiveService(ChenzaoContext context) : base(context)
       {
       }
   }
}
