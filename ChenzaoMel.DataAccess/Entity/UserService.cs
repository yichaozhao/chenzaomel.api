﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ChenzaoMel.DataAccess.Core;
using ChenzaoMel.Model.Database;

namespace ChenzaoMel.DataAccess.Entity {
    public class UserService : DalService {
      

        public async Task<User> ReadUser(string id)
        {
             return await    _context.Users.Where(s => s.Id == id).FirstOrDefaultAsync();
        }

        public UserService(ChenzaoContext context) : base(context)
        {
        }
    }
}
