﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.Model.Database;
using ChenzaoMel.Model.Util;

namespace ChenzaoMel.DataAccess.Core {
   public abstract class DalService
   {
       protected ChenzaoContext _context;
       public DalService(ChenzaoContext context)
       {
           _context = context;
       }
         ICollection<DailyObjective> ReadCurrentDailyObjective(User user) {
            using (var db = new ChenzaoContext()) {
                var dailyGoals = db.Objectives.OfType<DailyObjective>().Where(
                                                              s =>
                                                              string.Equals(s.UserId.ToLower(), user.Id.ToString())).OrderByDescending(s => s.Date == DateTime.Now.Date.ToMelDateTime()).ToList();
                 return dailyGoals;

            }
        }

       
    }
}
