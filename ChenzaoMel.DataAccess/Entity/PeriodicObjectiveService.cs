﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.Model.Database;
using ChenzaoMel.Model.Util;

namespace ChenzaoMel.DataAccess.Core {
    class PeriodicObjectiveService :DalService{
       public async Task<PeriodicalObjective> ReadPeriodicalObjective(User user, DateTime dateTime) {
                // Active & Within the peroid && Right Persion
           var time = dateTime.Date.ToMelDateTime();
                var dailyGoal = await _context.Objectives.OfType<PeriodicalObjective>().Include("Period").Where(
                                                             s =>
                                                             string.Equals(s.UserId, user.Id.ToString())).OrderByDescending(s => time <= s.Period.EndDate && time <= s.Period.StartDate).FirstOrDefaultAsync();
                return dailyGoal;
        }

        public PeriodicObjectiveService(ChenzaoContext context) : base(context)
        {
        }
    }
}
