﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.BusinessLogic.Util;

namespace ChenzaoMel.BusinessLogic.Core {
    public abstract class BusinessAbs {
        protected Logger _Logger;
        protected string _ipAddress;
        protected BusinessAbs(Logger logger) {
            this._Logger = logger;
            this._ipAddress = logger.IpAddress;
        }

        protected BusinessAbs(string ipAddress) {
            this._Logger = Logger.ObtainServerLogger(ipAddress);
            this._ipAddress = ipAddress;
        }
    }
}
