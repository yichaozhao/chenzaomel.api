﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ChenzaoMel.Interface.Core;
using ChenzaoMel.Model.Base;
using ChenzaoMel.Model.Core;
using ChenzaoMel.Model.Database;
using System.Web;
using System.Xml;
using ChenzaoMel.BusinessLogic.Util;
using ChenzaoMel.DataAccess.Facade;
using ChenzaoMel.Model.Core.Exception;
using ChenzaoMel.Model.Util;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Runtime.Caching;
using ChenzaoMel.DataAccess;

namespace ChenzaoMel.BusinessLogic.Core {
    public class WechatMessageService : BusinessAbs, IMessageService {
        private const string TEMPLATE_NO_OF_DAY = "[no_of_day]";
        private const string TEMPLATE_DATE_OF_TODAY = "[date_of_today]";
        private const string TEMPLATE_LEVEL_OF_ACHIEVEMENT = "[level_of_achievement]";
        private const string TEMPLATE_LEVEL_OF_HAPPNIESS = "[level_of_happniess]";
        private const string TEMPLATE_PEROID_OBJECTIVE_CONTENT = "[periodic_object_content]";
        private const string TEMPLATE_FROM_DATE = "[from_date]";
        private const string TEMPLATE_TO_DATE = "[to_date]";
        private const string TEMPLATE_DAILY_OBJECTIVE_CONTENT = "[daily_objective_content]";
        private const string TEMPLATE_EXPERIENCE = "[today_personal_experience]";
        private const string TEMPLATE_LUCK = "[today_luck]";
        private const string TEMPALTE_NO_OF_DAILY_OBJECTIVES = "[no_of_objectives]";
        private const string TEMPALTE_NO_OF_DAILY_COMPLETE = "[no_of_complete]";
        private const string TEMPALTE_NO_OF_DAYS_BEHAVIOUR_CHANGE = "[total_change_behaviour_days]";

        private const string DATE_PATTERN = "dd/MM/yyyy";
        private readonly string START_DATE = ConfigurationManager.AppSettings["StartDate"];
        private readonly string CHNAGE_BEHAVIOUR_PERIOD = ConfigurationManager.AppSettings["ChangeBehaviourPeriod"];
        private readonly string CMD_DAILYOBJECTIVE = ConfigurationManager.AppSettings["CmdDailyObjective"];
        private readonly string CMD_FEELING = ConfigurationManager.AppSettings["CmdFeeling"];
        private readonly string CMD_LUCKINESS = ConfigurationManager.AppSettings["CmdLuckiness"];
        private readonly string CMD_ACHIEVEMENT = ConfigurationManager.AppSettings["CmdAchievement"];
        private readonly string CMD_HAPPINESS = ConfigurationManager.AppSettings["CmdHappiness"];
        private readonly string CMD_COMPLETE = ConfigurationManager.AppSettings["CmdComplete"];
        private readonly string CMD_LOOKUPDAILY = ConfigurationManager.AppSettings["CmdLoopupDaily"];
        private readonly string CMD_LONGOBJECTIVE = ConfigurationManager.AppSettings["CmdLongObjective"];
        private readonly string CMD_HELP = ConfigurationManager.AppSettings["CmdHelp"];
        private readonly string CMD_BIND = ConfigurationManager.AppSettings["CmdBind"];
        private readonly string NO_GOAL_SPECIFIED_MESSAGE = ConfigurationManager.AppSettings["NoGoalSpecified"];
        private readonly string OBJECT_TEMPLATE = ConfigurationManager.AppSettings["DailyObjTemplate"];
        private readonly string TEMPLATE_HELP = ConfigurationManager.AppSettings["HelpTemplate"];
        private readonly string UPDATE_SUCCESS_CONTENT = ConfigurationManager.AppSettings["SuccessMsg"];

        public WechatMessageService(Logger logger) : base(logger) {

        }

        public WechatMessageService(string ipAddress) : base(ipAddress) {
        }

        //        public async Task<WeChatTextMsg> ReadCurrentPeriodObjective(WeChatTextMsg textMsg, DateTime date) {
        //            using (var db = new ChenzaoContext()) {
        ////                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/periodObjTemplate.xml");
        ////                var content = string.Empty;
        ////                using (var streamReader = new StreamReader(path, Encoding.UTF8)) {
        ////                    content = streamReader.ReadToEnd();
        ////                }
        //                string content = OBJECT_TEMPLATE;
        ////                XmlDocument xmlDoc = new XmlDocument();
        ////                xmlDoc.Load(OBJECT_TEMPLATE);
        //                User user =
        //                    await new UserObjectiveService(db).ReadUserPerioidicObjectives(textMsg.FromUserName, date);
        //                if (user.CurrentPeriodicalGoal == null) {
        //                    await
        //                        _Logger.WarnAsync(
        //                                          $"{textMsg.FromUserName} does not have periodical goal on {DateTime.Now.Date.ToMelDateTime().ToString("dd/MM")} ");
        //                    return new WeChatTextMsg() {
        //                        CreateTime = WechatHttpUtil.GetCreateTime(),
        //                        FromUserName = textMsg.FromUserName,
        //                        Content = "现阶段还没有订要养成的习惯哦。",
        //                        MsgType = MsgType.Text,
        //                        ToUserName = textMsg.ToUserName,
        //                    };
        //                }
        //
        ////                string templateContent = xmlDoc.SelectSingleNode("template/content").InnerText;
        //
        //                templateContent.Replace(TEMPLATE_FROM_DATE,
        //                                        user.CurrentPeriodicalGoal.Period.StartDate.Date.ToMelDateTime().ToString(
        //                                                                                                                  "dd/MM/yyyy"));
        //                templateContent.Replace(TEMPLATE_TO_DATE,
        //                                        user.CurrentPeriodicalGoal.Period.EndDate.Date.ToMelDateTime().ToString(
        //                                                                                                                "dd/MM/yyyy"));
        //                templateContent.Replace(TEMPLATE_PEROID_OBJECTIVE_CONTENT, user.CurrentPeriodicalGoal.Content);
        //                return new WeChatTextMsg() {
        //                    CreateTime = WechatHttpUtil.GetCreateTime(),
        //                    FromUserName = textMsg.FromUserName,
        //                    Content = templateContent,
        //                    MsgType = MsgType.Text,
        //                    ToUserName = textMsg.ToUserName,
        //                };
        //            }
        //        }

        private async Task<string> GenerateDailyObjectiveFromTemplate(string openId, DateTime dateTime) {
            using (var db = new ChenzaoContext()) {
                User user = await new UserObjectiveService(db).ReadUserAllObjectives(openId, dateTime);

                //                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/dailyObjTemplat.xml");
                var templateContent = OBJECT_TEMPLATE;

                //                using (var streamReader = new StreamReader(path, Encoding.UTF8)) {
                //                    templateContent = streamReader.ReadToEnd();
                //                }


                if (user.CurrentDailyGoals == null) {
                    string msg =
                        $"{openId} does not have daily goal on {dateTime.Date.ToMelDateTime().ToString("dd/MM")} ";
                    await
                        _Logger.WarnAsync(
                                          $"{openId} does not have daily goal on {dateTime.Date.ToMelDateTime().ToString("dd/MM")} ");
                    return ConfigurationManager.AppSettings["NoDailyGoalMsg"];
                }

                double dateDifference =
                    ((dateTime.Date - DateTime.ParseExact(START_DATE, DATE_PATTERN, CultureInfo.InvariantCulture))
                         .TotalDays % int.Parse(CHNAGE_BEHAVIOUR_PERIOD)) + 1;
                templateContent = templateContent.Replace(TEMPLATE_NO_OF_DAY, ((int)dateDifference).ToString());
                templateContent = templateContent.Replace(TEMPALTE_NO_OF_DAYS_BEHAVIOUR_CHANGE,
                                                          CHNAGE_BEHAVIOUR_PERIOD);
                templateContent = templateContent.Replace(TEMPLATE_DATE_OF_TODAY,
                                                          dateTime.Date.ToMelDateTime().ToString("dd/MM/yyyy"));
                templateContent = templateContent.Replace(TEMPLATE_LEVEL_OF_ACHIEVEMENT,
                                                          user.CurrentDailyGoals.AchievementLevel.GetValueOrDefault()
                                                              .ToString());
                templateContent = templateContent.Replace(TEMPLATE_LEVEL_OF_HAPPNIESS,
                                                          user.CurrentDailyGoals.HappynessLevel.GetValueOrDefault()
                                                              .ToString());
                templateContent = templateContent.Replace(TEMPLATE_EXPERIENCE, user.CurrentDailyGoals.PersonalFeeling);
                templateContent = templateContent.Replace(TEMPLATE_LUCK, user.CurrentDailyGoals.Luckiness);
                templateContent = templateContent.Replace(TEMPALTE_NO_OF_DAILY_OBJECTIVES,
                                                          user.CurrentDailyGoals.DailyObjectiveDetails?.Count
                                                              .ToString() ?? "0");
                templateContent = templateContent.Replace(TEMPALTE_NO_OF_DAILY_COMPLETE,
                                                          user.CurrentDailyGoals.DailyObjectiveDetails?.Where(
                                                                                                              s =>
                                                                                                              s
                                                                                                                  .Complete)
                                                              .ToList().Count.ToString() ?? "0");
                templateContent = templateContent.Replace(TEMPLATE_PEROID_OBJECTIVE_CONTENT,
                                                          user.CurrentPeriodicalGoal?.Content ?? "");
                StringBuilder sb = new StringBuilder();
                if (user.CurrentDailyGoals.DailyObjectiveDetails != null)
                    foreach (var dailyObjectiveDetail in user.CurrentDailyGoals.DailyObjectiveDetails.OrderBy(s=>s.Count).ToList()) {
                        sb.Append($"{dailyObjectiveDetail.Count}. {dailyObjectiveDetail.Content}");
                        if (dailyObjectiveDetail.Complete) {
                            sb.AppendLine("(已完成)");
                        } else {
                            sb.AppendLine("");
                        }
                    }
                templateContent = templateContent.Replace(TEMPLATE_DAILY_OBJECTIVE_CONTENT, sb.ToString());
                return templateContent;
            }

        }

        //        public async Task<WeChatTextMsg> ReadCurrentDailyObjective(WeChatTextMsg textMsg) {

        //            User user = await new UserObjectiveService().ReadUserAllObjectives(textMsg.FromUserName);
        //            if (user.CurrentDailyGoals == null ) {
        //                await _Logger.WarnAsync($"{textMsg.FromUserName} does not have daily goal on {DateTime.Now.Date.ToMelDateTime().ToString("dd/MM")} ");
        //                return new WeChatTextMsg() {
        //                    CreateTime = WechatHttpUtil.GetCreateTime(),
        //                    FromUserName = textMsg.FromUserName,
        //                    Content = "今天还没有给自己定目标哦。",
        //                    MsgType = MsgType.Text,
        //                    ToUserName = textMsg.ToUserName,
        //                };
        //            }
        //
        //            string templateContent = xmlDoc.SelectSingleNode("template/content").InnerText;
        //
        //            templateContent.Replace(TEMPLATE_NO_OF_DAY,
        //                                    ConfigurationManager.AppSettings["ObjPeriod"]);
        //            templateContent.Replace(TEMPLATE_DATE_OF_TODAY,
        //                                  DateTime.Now.Date.ToMelDateTime().ToString("dd/MM/yyyy"));
        //            templateContent.Replace(TEMPLATE_LEVEL_OF_ACHIEVEMENT,
        //                                            user.CurrentDailyGoals.AchievementLevel.ToString());
        //            templateContent.Replace(TEMPLATE_LEVEL_OF_HAPPNIESS,
        //                                            user.CurrentDailyGoals.HappynessLevel.ToString());
        //            templateContent.Replace(TEMPLATE_EXPERIENCE,
        //                                            user.CurrentDailyGoals.PersonalFeeling);
        //            templateContent.Replace(TEMPLATE_LUCK,
        //                                            user.CurrentDailyGoals.Luckiness.ToString());
        //          templateContent.Replace(TEMPALTE_NO_OF_DAILY_OBJECTIVES,
        //                                            user.CurrentDailyGoals.DailyObjectiveDetails.Count.ToString());
        //            templateContent.Replace(TEMPALTE_NO_OF_DAILY_COMPLETE,
        //                                           user.CurrentDailyGoals.DailyObjectiveDetails.Where(s=>s.Complete).ToList().Count.ToString());
        //            templateContent.Replace(TEMPLATE_PEROID_OBJECTIVE_CONTENT,
        //                                           user.CurrentPeriodicalGoal.Content.ToString());
        //            StringBuilder sb = new StringBuilder();
        //            foreach (var dailyObjectiveDetail in user.CurrentDailyGoals.DailyObjectiveDetails)
        //            {
        //                sb.AppendLine($"{dailyObjectiveDetail.Count}. {dailyObjectiveDetail.Content}");
        //            }
        //            templateContent.Replace(TEMPLATE_DAILY_OBJECTIVE_CONTENT,
        //                                           sb.ToString());
        //            return new WeChatTextMsg() {
        //                CreateTime = WechatHttpUtil.GetCreateTime(),
        //                FromUserName = textMsg.FromUserName,
        //                Content = templateContent,
        //                MsgType = MsgType.Text,
        //                ToUserName = textMsg.ToUserName,
        //            };
        //        }


        //        public Task<WeChatTextMsg> AddOrUpdateDailyGoal(WeChatTextMsg msg) {
        //            return new WeChatTextMsg() {
        //                CreateTime = WechatHttpUtil.GetCreateTime(),
        //                FromUserName = textMsg.FromUserName,
        //                Content = templateContent,
        //                MsgType = MsgType.Text,
        //                ToUserName = textMsg.ToUserName,
        //            };
        //        }

        public async Task<WeChatTextMsg> ProcessText(WeChatTextMsg msg) {
            WeChatTextMsg ret = new WeChatTextMsg();
            try {
                DateTime date;
                string content = string.Empty;
                // Cache Locker

                if (msg.Content.StartsWith("[")) {
                    int index1 = msg.Content.IndexOf("]"); // Content [starttime]
                    string dateStr = msg.Content.Substring(1, index1 - 1);
                    content = msg.Content.Remove(0, index1 + 1);
                    date = DateTime.ParseExact(dateStr, DATE_PATTERN, CultureInfo.InvariantCulture);
                } else {
                    date = DateTime.Now.ToMelDateTime();
                    content = msg.Content;
                }

                //13/07/2016 v1.0.2   EZ  Support traditional Chinese for command line
                if (StringUtil.StartsWithArrayString(content, CMD_DAILYOBJECTIVE)) {
                    // Update Daily Objective
                    var cmdLength = CMD_DAILYOBJECTIVE.Split(new[] { "," }, StringSplitOptions.None)[0].Length;
                    int number = int.Parse(content.Substring(cmdLength, 1));
                    string c = content.Substring(cmdLength + 2, content.Length - cmdLength - 2);
                    KeyValuePair<int, string> objective = new KeyValuePair<int, string>(number, c);


                    await AddOrUpdateDailyGoal(msg.ToUserName, objective, date);
                    string returnContent = await GenerateDailyObjectiveFromTemplate(msg.ToUserName, date);
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = returnContent,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };
                    //13/07/2016 v1.0.2   EZ  Support traditional Chinese for command line
                } else if (StringUtil.StartsWithArrayString(content, CMD_ACHIEVEMENT)) {
                    int levelAchievement =
                        int.Parse(content.Substring(CMD_ACHIEVEMENT.Length + 1,
                                                    content.Length - CMD_ACHIEVEMENT.Length - 1));

                    await AddOrUpdateDailyAchievementLevel(msg.ToUserName, levelAchievement, date);
                    string returnContent = await GenerateDailyObjectiveFromTemplate(msg.ToUserName, date);
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = returnContent,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };
                    //13/07/2016 v1.0.2   EZ  Support traditional Chinese for command line
                } else if (StringUtil.StartsWithArrayString(content, CMD_FEELING)) {
                    string luckiness = content.Substring(CMD_FEELING.Length + 1,
                                                         content.Length - CMD_FEELING.Length - 1);
                    await AddOrUpdateDailyFeeling(msg.ToUserName, luckiness, date);
                    string returnContent = await GenerateDailyObjectiveFromTemplate(msg.ToUserName, date);
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = returnContent,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };
                    //                    int levelFeeling = int.Parse(content.Substring(CMD_FEELING.Length + 2,
                    //                                                content.Length - CMD_FEELING.Length - 2));
                    //                    await AddOrUpdateDailyFeelingLevel()
                    //13/07/2016 v1.0.2   EZ  Support traditional Chinese for command line
                } else if (StringUtil.StartsWithArrayString(content, CMD_LUCKINESS)) {
                    string luckiness =
                        content.Substring(CMD_LUCKINESS.Split(new[] { "," }, StringSplitOptions.None)[0].Length + 1,
                                          content.Length -
                                          CMD_LUCKINESS.Split(new[] { "," }, StringSplitOptions.None)[0].Length - 1);
                    await AddOrUpdateDailyLuckiness(msg.ToUserName, luckiness, date);
                    string returnContent = await GenerateDailyObjectiveFromTemplate(msg.ToUserName, date);
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = returnContent,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };
                    //                    int levelFeeling = int.Parse(content.Substring(CMD_FEELING.Length + 2,
                    //                                                content.Length - CMD_FEELING.Length - 2));
                    //                    await AddOrUpdateDailyFeelingLevel()
                } else if (StringUtil.StartsWithArrayString(content, CMD_HAPPINESS)) {
                    int levelHappiness =
                        int.Parse(content.Substring(CMD_HAPPINESS.Length + 1,
                                                    content.Length - CMD_HAPPINESS.Length - 1));

                    await AddOrUpdateDailyHappinessLevel(msg.ToUserName, levelHappiness, date);
                    string returnContent = await GenerateDailyObjectiveFromTemplate(msg.ToUserName, date);
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = returnContent,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };
                } else if (StringUtil.StartsWithArrayString(content, CMD_LOOKUPDAILY)) {
                    string returnContent = await GenerateDailyObjectiveFromTemplate(msg.ToUserName, date);
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = returnContent,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };
                } else if (StringUtil.StartsWithArrayString(content, CMD_COMPLETE)) {
                    int taskCount =
                        int.Parse(content.Substring(CMD_COMPLETE.Length, content.Length - CMD_HAPPINESS.Length));
                    await UpdateDailyCompleteFlag(msg.ToUserName, taskCount, date);
                    string returnContent = await GenerateDailyObjectiveFromTemplate(msg.ToUserName, date);
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = returnContent,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };
                } else if (StringUtil.StartsWithArrayString(content, CMD_LONGOBJECTIVE)) {

                } else if (StringUtil.StartsWithArrayString(content, CMD_BIND)) {
                    //                    int taskCount =
                    //                      int.Parse(content.Substring(CMD_COMPLETE.Length, content.Length - CMD_HAPPINESS.Length));
                    string name =
                        content.Substring(CMD_BIND.Split(new[] { "," }, StringSplitOptions.None)[0].Length + 1,
                                          content.Length -
                                          CMD_BIND.Split(new[] { "," }, StringSplitOptions.None)[0].Length - 1);
                    await UpdateUserName(msg.ToUserName, name);
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = UPDATE_SUCCESS_CONTENT,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };

                } else if (StringUtil.StartsWithArrayString(content, CMD_HELP)) {
                    return new WeChatTextMsg() {
                        CreateTime = WechatHttpUtil.GetCreateTime(),
                        FromUserName = msg.FromUserName,
                        Content = TEMPLATE_HELP,
                        MsgType = MsgType.Text,
                        ToUserName = msg.ToUserName,
                    };
                }
            } catch (ChenzaoException e) {
                return new WeChatTextMsg() {
                    CreateTime = WechatHttpUtil.GetCreateTime(),
                    FromUserName = msg.FromUserName,
                    Content = e.Message,
                    MsgType = MsgType.Text,
                    ToUserName = msg.ToUserName,
                };
            } catch (Exception ex) {
                await _Logger.ErrorAsync(ex);
            }
            return new WeChatTextMsg() {
                CreateTime = WechatHttpUtil.GetCreateTime(),
                FromUserName = msg.FromUserName,
                Content = "请输入“帮助”查看可用命令",
                MsgType = MsgType.Text,
                ToUserName = msg.ToUserName,
            };
        }

        private async Task AddOrUpdateDailyFeeling(string openId, string feeling, DateTime date) {
            using (var db = new ChenzaoContext()) {
                var user = await new UserObjectiveService(db).ReadUserDailyObjectives(openId, date);
                if (user.CurrentDailyGoals == null) {
                    // The user does not have a goal
                    var newDailyGoal = new DailyObjective() {
                        UserId = user.Id,
                        Date = date.Date.ToMelDateTime(),
                        PersonalFeeling = feeling
                    };
                    db.Objectives.Add(newDailyGoal);
                    await db.SaveChangesAsync();
                } else {
                    // Update luckiness Level
                    user.CurrentDailyGoals.PersonalFeeling = feeling;
                    db.Objectives.AddOrUpdate(s => s.Id, user.CurrentDailyGoals);
                }
                await db.SaveChangesAsync();

            }
        }

        private async Task UpdateDailyCompleteFlag(string openId, int taskCount, DateTime date) {
            try {
                using (var db = new ChenzaoContext()) {
                    var user = await new UserObjectiveService(db).ReadUserDailyObjectives(openId, date);
                    var chosenGoal =
                        user.CurrentDailyGoals.DailyObjectiveDetails.FirstOrDefault(x => x.Count == taskCount);
                    chosenGoal.Complete = true;
                    await db.SaveChangesAsync();
                }
            } catch (Exception ex) {
                throw new ChenzaoException(NO_GOAL_SPECIFIED_MESSAGE);
            }
        }

        private async Task AddOrUpdateDailyAchievementLevel(string openId, int levelAchievement, DateTime date) {
            using (var db = new ChenzaoContext()) {
                var user = await new UserObjectiveService(db).ReadUserDailyObjectives(openId, date);
                if (user.CurrentDailyGoals == null) {
                    // The user does not have a goal
                    var newDailyGoal = new DailyObjective() {
                        UserId = user.Id,
                        Date = date.Date.ToMelDateTime(),
                        AchievementLevel = levelAchievement
                    };
                    db.Objectives.Add(newDailyGoal);
                    await db.SaveChangesAsync();
                } else {
                    // Update luckiness Level
                    user.CurrentDailyGoals.AchievementLevel = levelAchievement;
                    db.Objectives.AddOrUpdate(s => s.Id, user.CurrentDailyGoals);
                }
                await db.SaveChangesAsync();
            }
        }

        private async Task AddOrUpdateDailyHappinessLevel(string openId, int happniessLevel, DateTime date) {
            using (var db = new ChenzaoContext()) {
                var user = await new UserObjectiveService(db).ReadUserDailyObjectives(openId, date);
                if (user.CurrentDailyGoals == null) {
                    // The user does not have a goal
                    var newDailyGoal = new DailyObjective() {
                        UserId = user.Id,
                        Date = date.Date.ToMelDateTime(),
                        HappynessLevel = happniessLevel
                    };
                    db.Objectives.Add(newDailyGoal);
                    await db.SaveChangesAsync();
                } else {
                    // Update luckiness Level
                    user.CurrentDailyGoals.HappynessLevel = happniessLevel;
                    db.Objectives.AddOrUpdate(s => s.Id, user.CurrentDailyGoals);
                }
                await db.SaveChangesAsync();

            }
        }

        private async Task AddOrUpdateDailyLuckiness(string openId, string luckiness, DateTime date) {
            using (var db = new ChenzaoContext()) {
                var user = await new UserObjectiveService(db).ReadUserDailyObjectives(openId, date);
                if (user.CurrentDailyGoals == null) {
                    // The user does not have a goal
                    var newDailyGoal = new DailyObjective() { UserId = user.Id, Date = date.Date.ToMelDateTime(), Luckiness = luckiness };
                    db.Objectives.Add(newDailyGoal);
                    await db.SaveChangesAsync();
                } else {
                    // Update luckiness Level
                    user.CurrentDailyGoals.Luckiness = luckiness;
                    db.Objectives.AddOrUpdate(s => s.Id, user.CurrentDailyGoals);
                }
                await db.SaveChangesAsync();

            }
        }

        private async Task UpdateUserName(string openId, string userName) {
            using (var db = new ChenzaoContext()) {
                var user = await db.Users.FirstOrDefaultAsync(x => x.Id == openId);
                user.Name = userName;
                await db.SaveChangesAsync();
            }
        }

        public async Task AddOrUpdateDailyGoal(string openId, KeyValuePair<int, string> objective, DateTime date) {
            using (var db = new ChenzaoContext()) {
                using (var dbContextTransaction = db.Database.BeginTransaction()) {
                    try {

                        var user = await new UserObjectiveService(db).ReadUserDailyObjectives(openId, date);
                        DailyObjective dailyGoal = user.CurrentDailyGoals;
                        _Logger.Info($"Read daily goal for {openId}");
                        if (dailyGoal == null) {
                            _Logger.Info($"Daily goal does not exist for {openId}");
                            dailyGoal = new DailyObjective() { UserId = user.Id, Date = date.Date.ToMelDateTime() };
                        }

                        DailyObjectiveDetail detail = null;
                        _Logger.Info($"Read detail goal for {openId}");
                        detail = dailyGoal.DailyObjectiveDetails?.FirstOrDefault(s => s.Count == objective.Key);

                        if (detail == null) {
                            _Logger.Info($"Detail daily goal does not exist for {openId}");
                            detail = new DailyObjectiveDetail() { Content = objective.Value, Count = objective.Key };
                            if (dailyGoal.DailyObjectiveDetails == null) {
                                dailyGoal.DailyObjectiveDetails = new List<DailyObjectiveDetail>() { detail };
                            } else {
                                dailyGoal.DailyObjectiveDetails.Add(detail);
                            }
                        } else {
                            detail.Content = objective.Value;
                            detail.Complete = false;
                        }
                        db.Objectives.AddOrUpdate(dailyGoal);
                        await db.SaveChangesAsync();
                        dbContextTransaction.Commit();
                    } catch (Exception ex) {
                        await _Logger.ErrorAsync($"AddOrUpdateDailyGoal Error: {ex.ToString()}");
                        dbContextTransaction.Rollback();
                    }

                    //                da
                    //
                    //                if (user.CurrentDailyGoals == null) {
                    //                    // The user does not have a goal
                    //                    var newDailyGoal = new DailyObjective() { UserId = user.Id, Date = date.Date.ToMelDateTime(), };
                    //                    db.Objectives.Add(newDailyGoal);
                    //                    await db.SaveChangesAsync();
                    //                    user = await new UserObjectiveService(db).ReadUserDailyObjectives(openId, date);
                    //                }}
                    //
                    //                if (user.CurrentDailyGoals == null || user.CurrentDailyGoals.DailyObjectiveDetails.Count == 0) {
                    //                    var newGoalDetail = new DailyObjectiveDetail() { DailyObjectiveId = user.CurrentDailyGoals.Id, Content = objective.Value, Count = 1 };
                    //                    db.Objectives.Add(newGoalDetail);
                    //                    await db.SaveChangesAsync();
                    //                } else {
                    //                    var chosenGoal =
                    //                        user.CurrentDailyGoals.DailyObjectiveDetails.FirstOrDefault(x => x.Count == objective.Key);
                    //                    if (chosenGoal == null) {
                    //                        db.Objectives.Add(new DailyObjectiveDetail() {
                    //                            Count = user.CurrentDailyGoals.DailyObjectiveDetails.Count + 1,
                    //                            Content = objective.Value,
                    //                            DailyObjectiveId = user.CurrentDailyGoals.Id
                    //                        });
                    //                    } else {
                    //                        chosenGoal.Content = objective.Value;
                    //                        chosenGoal.Complete = false;
                    //                    }
                    //
                    //                    await db.SaveChangesAsync();

                }
            }
        }
    }
}
