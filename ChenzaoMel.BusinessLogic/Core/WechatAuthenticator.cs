﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.Interface.Core;
using ChenzaoMel.Model.Core;
using ChenzaoMel.Model.DataTransfer;

namespace ChenzaoMel.BusinessLogic.Core {
    public class WechatAuthenticator : IAuthenticator{
        public string GenerateSignature(AuthRequest request)
        {
            var charList = new List<string> {request.Token, request.Timestamp, request.SaltedString};
            var sortedStr = string.Join("", charList.OrderBy(s => s));
            

            // SHA1 Encoding
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = BitConverter.ToString(sha1.ComputeHash(Encoding.UTF8.GetBytes(sortedStr)));
                return hash.Replace(@"-", "").ToLower();
            }
        }
    }
}
