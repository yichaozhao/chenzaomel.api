﻿using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.DataAccess;
using ChenzaoMel.Interface.Util;
using ChenzaoMel.Model.Core;
using ChenzaoMel.Model.Database;
using ChenzaoMel.Model.Util;

namespace ChenzaoMel.BusinessLogic.Util {
    public class Logger : ILogger
    {
        public string _ipAddress;

        public string IpAddress
        {
            get { return _ipAddress; }
            set { _ipAddress = value; }
        }

        private static Logger _logger;
        public static Logger  ObtainServerLogger(string ip){
            if (_logger == null)
            {
                _logger = new Logger(ip);
                return _logger;
            } else
            {
                _logger._ipAddress = ip;
                return _logger;
            }
        }
 public static Logger  ObtainServerLogger(){
            if (_logger == null)
            {
                _logger = new Logger("");
                return _logger;
            } else
            {
                _logger._ipAddress = "";
                return _logger;
            }
        }

        private Logger(string ipAddress)
        {
            this._ipAddress = ipAddress;
        }
        public async Task ErrorAsync(Exception ex, [CallerMemberName]string callerName = "" ,[CallerLineNumber] int lineNumber = 0)
        {
           await LogAsync(FormatException(ex), ErrorLevel.ERROR, callerName, lineNumber);
        }

        public async Task WarnAsync(Exception ex, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
            await LogAsync(FormatException(ex), ErrorLevel.WARN, callerName, lineNumber);

        }

        public async Task DebugAsync(Exception ex, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
            await LogAsync(FormatException(ex), ErrorLevel.DEBUG, callerName, lineNumber);

        }

        public async Task InfoAsync(Exception ex, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
            await LogAsync(FormatException(ex), ErrorLevel.INFO, callerName, lineNumber);

        }

        public async Task ErrorAsync(string content, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
            await LogAsync(content, ErrorLevel.ERROR, callerName, lineNumber);
        }

        public async Task WarnAsync(string content, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
            await LogAsync(content, ErrorLevel.WARN, callerName, lineNumber);

        }

        public async Task DebugAsync(string content, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
            await LogAsync(content, ErrorLevel.DEBUG, callerName, lineNumber);

        }

        public async Task InfoAsync(string content, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
            await LogAsync(content, ErrorLevel.INFO, callerName, lineNumber);

        }

     
    
        public void Error(Exception ex, [CallerMemberName]string callerName = "" ,[CallerLineNumber] int lineNumber = 0)
        {
            Log(FormatException(ex), ErrorLevel.ERROR, callerName, lineNumber);
        }

        public void Warn(Exception ex, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
             Log(FormatException(ex), ErrorLevel.WARN, callerName, lineNumber);

        }

        public void Debug(Exception ex, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
             Log(FormatException(ex), ErrorLevel.DEBUG, callerName, lineNumber);

        }

        public void Info(Exception ex, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
             Log(FormatException(ex), ErrorLevel.INFO, callerName, lineNumber);

        }

        public void Error(string content, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
             Log(content, ErrorLevel.ERROR, callerName, lineNumber);
        }

        public void Warn(string content, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
             Log(content, ErrorLevel.WARN, callerName, lineNumber);

        }

        public void Debug(string content, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
             Log(content, ErrorLevel.DEBUG, callerName, lineNumber);

        }

        public void Info(string content, [CallerMemberName]string callerName = "", [CallerLineNumber] int lineNumber = 0)
        {
             Log(content, ErrorLevel.INFO, callerName, lineNumber);

        }

     

        private string FormatException(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ex.ToString());
            do {
                    sb.AppendLine(ex.Message);
                }
                while ((ex = ex.InnerException) != null);
            return sb.ToString();
        }
  
        private async Task LogAsync(string content, ErrorLevel errorLevel, string method, int line) {
            using (var db = new ChenzaoContext())
            {
                Log log = new Log();
                log.Content = content;
                log.ErrorLevel = errorLevel;
                log.TimeStamp = DateTime.Now.ToMelDateTime();
                log.IpAddress = this._ipAddress;
                db.Logs.Add(log);
               await   db.SaveChangesAsync();
            }
        }
        private  void Log(string content, ErrorLevel errorLevel, string method, int line) {
            using (var db = new ChenzaoContext())
            {
                Log log = new Log();
                log.Content = content;
                log.ErrorLevel = errorLevel;
                log.IpAddress = this._ipAddress;
                db.Logs.Add(log);
                db.SaveChanges();
            }
        }

       
    }
}
