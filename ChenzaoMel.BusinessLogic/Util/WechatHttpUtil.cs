﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.DataAccess;
using ChenzaoMel.Model.Util;

namespace ChenzaoMel.BusinessLogic.Util {
    public class WechatHttpUtil {
        public static HttpResponseMessage GetWechatAuthResponseMessage(string strMsg) {
            HttpResponseMessage result = new HttpResponseMessage { Content = new StringContent(strMsg, Encoding.GetEncoding("UTF-8"), "application/x-www-form-urlencoded") }; return result;
        }

        public static async Task<HttpResponseMessage> GetWechatAuthResponseMessageAsync(string strMsg) {
            return await Task<HttpResponseMessage>.Factory.StartNew(() => new HttpResponseMessage {
                Content =
                                                                                  new StringContent(strMsg,
                                                                                                    Encoding
                                                                                                        .GetEncoding
                                                                                                        ("UTF-8"),
                                                                                                    "application/x-www-form-urlencoded")
            });

        }


        public static HttpResponseMessage GetWechatXmlResponseMessage(string strXml) {
            return new HttpResponseMessage(HttpStatusCode.OK) {
                Content = new StringContent(strXml, Encoding.UTF8, "application/xml"),
            };
        }
        public static async Task<HttpResponseMessage> GetWechatXmlResponseMessageAsync(string strXml) {
            return await Task<HttpResponseMessage>.Factory.StartNew(() => new HttpResponseMessage {
                Content =new StringContent(strXml,Encoding.UTF8,"application/xml")
            });
        }
        private static readonly DateTime Jan1st1970 = new DateTime
(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long GetCreateTime() {
            return (long)(DateTime.UtcNow.ToBeiJinDateTime() - Jan1st1970).TotalMilliseconds / 1000;
        }
    }

    //        public static async Task<HttpResponseMessage> GetHttpResponseMessageAsync(string strMsg)
    //        {
    //            return  new HttpResponseMessage { Content = new StringContent(strMsg, Encoding.GetEncoding("UTF-8"), "application/x-www-form-urlencoded") }; return result;
    //        }
}
