﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChenzaoMel.Model.DataTransfer {
 public   class ObjectiveResponse {
     public string Name { get; set; }
     public string Date { get; set; }
     public string UserId { get; set; }
     public int NoOfObjectives { get; set; }
     public int NoOfCompletes { get; set; }
 }
}
