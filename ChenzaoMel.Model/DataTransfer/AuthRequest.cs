﻿namespace ChenzaoMel.Model.DataTransfer {
    public class AuthRequest {
        public string Timestamp { get; set; }
        public string Token { get; set; }
        public string SaltedString { get; set; }

    }
}
