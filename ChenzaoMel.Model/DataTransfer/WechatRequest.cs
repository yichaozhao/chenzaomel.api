﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChenzaoMel.Model.DataTransfer {
    public   class WechatRequest {
        public string Signature { get; set; }
        public DateTime Timestamp { get; set; }
        public long Nonce { get; set; }
        public string Echostr { get; set; }    
    }
}
