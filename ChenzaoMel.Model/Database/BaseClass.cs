﻿using System;
using ChenzaoMel.Model.Util;

namespace ChenzaoMel.Model.Database {
    public abstract class BaseClass {
        public long Id { get; set; }
        public string IpAddress { get; set; }
  
        public DateTime TimeStamp { get; set; }
        public bool IsActive { get; set; }
        protected BaseClass()
        {
            IpAddress = "192.168.1.1";
            TimeStamp = DateTime.Now.ToMelDateTime();
            IsActive = true;
        }
        
    }
}
