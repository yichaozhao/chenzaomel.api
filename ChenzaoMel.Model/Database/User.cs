﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ChenzaoMel.Model.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChenzaoMel.Model.Database {
   public class User {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public new string Id  { get; set; }

       public User()
       {
           IsActive = true;
       }
       public bool IsActive { get; set; }
        public string Name { get; set; }
        public virtual ICollection<PeriodicalObjective> PeroidicalGoals { get; set; }
       public virtual ICollection<DailyObjective> DailyGoals{ get; set; }
        [NotMapped]
       public virtual PeriodicalObjective CurrentPeriodicalGoal { get; set; }
        [NotMapped]
        public virtual DailyObjective CurrentDailyGoals { get; set; }
    }
}
