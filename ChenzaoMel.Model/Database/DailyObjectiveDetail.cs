﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChenzaoMel.Model.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChenzaoMel.Model.Database {
    public class DailyObjectiveDetail : Objective{
        public virtual DailyObjective DailyObjective { get; set; }
        [ForeignKey("DailyObjective")]
        public long DailyObjectiveId { get; set; }

        public int Count { get; set; }
    }
}
