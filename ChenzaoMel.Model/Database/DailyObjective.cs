﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ChenzaoMel.Model.Base;
using ChenzaoMel.Model.Core;

namespace ChenzaoMel.Model.Database {
  public  class DailyObjective : Objective{
      public DateTime Date { get; set; }
      public virtual User User { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        
      public int? AchievementLevel { get; set; }
      public int? HappynessLevel { get; set; }
      public virtual ICollection<DailyObjectiveDetail> DailyObjectiveDetails { get; set; }
      public string PersonalFeeling { get; set; }
      public string Luckiness { get; set; }
  }
}
