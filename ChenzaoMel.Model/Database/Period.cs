﻿using System;
using ChenzaoMel.Model.Base;
using ChenzaoMel.Model.Core;

namespace ChenzaoMel.Model.Database {
    public class Period : BaseClass{
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual PeriodicalObjective PeriodicalObjective { get; set; }

    }
}
