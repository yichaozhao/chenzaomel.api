﻿using ChenzaoMel.Model.Base;

namespace ChenzaoMel.Model.Database {
    public class Log :BaseClass{
//        public string Method { get; set; }
//        public string LineNumber { get; set; }
        public string Content { get; set; }
        public ErrorLevel ErrorLevel{ get; set; }
    }
}
