﻿
using ChenzaoMel.Model.Database;

namespace ChenzaoMel.Model.Base {
    public  abstract  class Objective : BaseClass{
        public string Content { get; set; }
        public bool Complete { get; set; }

        protected Objective()
        {
            Complete = false;
        }
    }
}
