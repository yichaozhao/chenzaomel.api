﻿using System.ComponentModel.DataAnnotations.Schema;
using ChenzaoMel.Model.Base;

namespace ChenzaoMel.Model.Database {
    public class PeriodicalObjective :Objective{
        public virtual Period Period { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
    }
}
