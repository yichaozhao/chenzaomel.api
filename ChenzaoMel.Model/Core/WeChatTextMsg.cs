﻿using System;
using System.Xml.Serialization;
using ChenzaoMel.Model.Base;

namespace ChenzaoMel.Model.Core {
    [XmlRoot("xml")]
    public  class WeChatTextMsg : WechatBaseMsg{
        public string Content { get; set; }
    }

    
}
