﻿using System.Xml.Serialization;

namespace ChenzaoMel.Model.Core {
        public enum MsgType {
            [XmlEnum("event")]
            Event,
            [XmlEnum("text")]
            Text,
            [XmlEnum("image")]
            Image,
            [XmlEnum("voice")]
            Voice,
            [XmlEnum("video")]
            Video,
            [XmlEnum("music")]
            Music,
            [XmlEnum("news")]
            News
    }
}
