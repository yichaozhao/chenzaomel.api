﻿using System.IO;
using System.Xml.Serialization;
using ChenzaoMel.Model.Util;

namespace ChenzaoMel.Model.Core {
    public abstract  class WechatBaseMsg {
        public string ToUserName { get; set; }

        public string FromUserName { get; set; }

        public long CreateTime { get; set; }

        public MsgType MsgType { get; set; }
        public string GenerateXml() {
            using (StringWriter sw = new Utf8StringWriter())
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                ns.Add("", ""); //去除命名空间
                XmlSerializer serializer = new XmlSerializer(this.GetType());

                serializer.Serialize(sw, this, ns);

                return sw.ToString();
            }
        }
    }
}
