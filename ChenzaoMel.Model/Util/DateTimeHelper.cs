﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChenzaoMel.Model.Util {
    public static class DateTimeHelper
    {
        public static DateTime ToMelDateTime(this DateTime time)
        {
            // Get the Melbourne Time zone
            TimeZoneInfo est = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
            DateTime targetTime = TimeZoneInfo.ConvertTime(time, est);
            return targetTime;


        }

        public static DateTime ToBeiJinDateTime(this DateTime time)
        {
            // Get the Melbourne Time zone
            TimeZoneInfo est = TimeZoneInfo.FindSystemTimeZoneById("China Standard Time");
            DateTime targetTime = TimeZoneInfo.ConvertTime(time, est);
            return targetTime;
        }
    }

}
