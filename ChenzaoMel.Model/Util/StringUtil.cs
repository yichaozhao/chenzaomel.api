﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChenzaoMel.Model.Util {
    public class StringUtil {
        public static bool StartsWithArrayString(string srcStr, string targetStr)
        {
            var flag = false;
            var strArr = targetStr.Split(new[] {","}, StringSplitOptions.None);
            foreach (var s in strArr)
            {
                flag |= srcStr.StartsWith(s);
            }
            return flag;
        }
    }
}
