﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChenzaoMel.Model.Core.Exception {
    public class ChenzaoException : System.Exception, ISerializable {
        public ChenzaoException()
         : base() { }

        public ChenzaoException(string message)
         : base(message) { }

        public ChenzaoException(string message, System.Exception inner) : base(message, inner) { }

        public ChenzaoException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
