﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ChenzaoMel.DataAccess;
using ChenzaoMel.Model.Core;
using ChenzaoMel.Model.Database;
using ChenzaoMel.Model.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChenzaoMel.Tests {
    [TestClass]
    public class CoreTest {
        [TestMethod]
        public void DbSchemaTest() {
            using (var db = new ChenzaoContext())
            {
                PeriodicalObjective obj = new PeriodicalObjective()
                                          {
                                              Period =
                                                  new Period()
                                                  {StartDate = DateTime.Now, EndDate = DateTime.Now.AddDays(1)},
                                              Complete = false, IpAddress = "192.168.1.1",
                                              TimeStamp =  DateTime.Now
                                          };
                db.Objectives.Add(obj);
                db.SaveChanges();
            }
        }

        [TestMethod]
        public void ChineseCharTest()
        {
            string testString = "测试：";
            Debug.WriteLine(testString.IndexOf("测"));
            Debug.WriteLine(testString.Substring(1,2));

        }

        [TestMethod]
        public void StringSplitTest()
        {
            string testStr="a";
            var s = testStr.Split(new[] {","}, StringSplitOptions.None);
            if (s.Contains(testStr))
            {
                Debug.WriteLine("True");
            }
        }

        [TestMethod()]
        public void StartsWithArrayStringTest()
        {
            Debug.WriteLine(StringUtil.StartsWithArrayString("目标", "目标,目標"));
        }
    }
}
