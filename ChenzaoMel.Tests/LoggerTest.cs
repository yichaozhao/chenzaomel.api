﻿using System;
using System.Threading.Tasks;
using ChenzaoMel.BusinessLogic;
using ChenzaoMel.BusinessLogic.Util;
using ChenzaoMel.Interface.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChenzaoMel.Tests {
    [TestClass]
    public class LoggerTest {
        [TestMethod]
        public async Task LogTest() {
            ILogger logger = Logger.ObtainServerLogger("192.168.1.1");
            await logger.ErrorAsync("TestError");
        }
    }
}
